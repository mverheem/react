import React from 'react';

const FlexImageCaption = ({caption}) => {
    return (<figcaption dangerouslySetInnerHTML={{__html: caption}}></figcaption>);
};
export default FlexImageCaption