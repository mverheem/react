import React from 'react';

const FlexText = ({text}) => {
    const imageContainer = (<section className="article-content">
        <div className="body-text">
            <div className="article-main-text" dangerouslySetInnerHTML={{__html: text}}></div>
        </div>
    </section>);

    return (
        imageContainer
    );
}

export default FlexText