import React from 'react';
import FlexImageCaption from './FlexImageCaption';

const FlexImage = ({image, caption}) => {
    const imageContainer = (
        <section className="article-content">
            <div className="body-text">
                <figure className='article-image'>
                    <div className="image-container" dangerouslySetInnerHTML={{__html: image}}></div>
                    <FlexImageCaption caption={caption}></FlexImageCaption>
                </figure>
            </div>
        </section>
    );

    return (
        imageContainer
    );
}

export default FlexImage