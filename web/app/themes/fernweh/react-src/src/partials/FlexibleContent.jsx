import React from 'react';
import FlexImage from './flexible-content/FlexImage';
import FlexText from './flexible-content/FlexText';

const FlexibleContent = (props) => {
  const results = props.flexible_content.map((item, i) => {
    const type = item._type;
    switch (type) {
      case 'text':
        return <div><FlexText text={item.text} key={i} /></div>;
        break;
      case 'image':
        return <FlexImage image={item.image} caption={item.caption} key={i} />;
        break;
      default:
        return null;
        break;
    }
    return '';
  });

  return results;
};

export default FlexibleContent;