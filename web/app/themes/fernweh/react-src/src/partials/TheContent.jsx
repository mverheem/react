import React from 'react';

const TheContent = (props) => {
    const theContent = props.content;
    const results = (
        <section>
            <div className="article-main-text" dangerouslySetInnerHTML={{__html: theContent}}></div>
        </section>)

    if (theContent) {
        return results;
    } else {
        return null;
    }

};

export default TheContent;