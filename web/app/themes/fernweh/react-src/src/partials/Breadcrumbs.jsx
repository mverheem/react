import React from 'react';
import {Link} from "react-router-dom";

const Breadcrumbs = (props) => {
    return (
        <ul className="breadcrumbs">
            <li className="item-home"><Link className="bread-link bread-home" to="/">Home</Link></li>
            <li className="separator separator-home"> &gt; </li>
            <li className="item-current item-16"><strong className="bread-current bread-16"> {props.title}</strong></li>
        </ul>
    )
}
export default Breadcrumbs;