import React, { useState } from 'react';
import { useScrollPosition } from '@n8tb1t/use-scroll-position';
import { Link } from 'react-router-dom';

function Header() {
  const [hideOnScroll, setHideOnScroll] = useState('');

  useScrollPosition(({ currPos }) => {
    if (currPos.y <= -500) setHideOnScroll(' header--fixed');
    else { setHideOnScroll(''); }
  }, [hideOnScroll]);


  return (
    <header className={`header${hideOnScroll}`}>
      <a className="header__sitename" href="/">Fernweh</a>
      <nav className="navigation">
        <div className="menu-main-container">
          <ul className="menu">
            <li className="menu-item"><Link to="/">Home </Link></li>
            <li className="menu-item"><Link to="/over-fernweh">Fernweh</Link></li>

            <li className="menu-item"><Link to="/blog">Blog</Link></li>
            <li className="menu-item"><Link to="/landen">Landen</Link></li>
            <li className="menu-item"><Link to="/over-mij">Over mij</Link></li>
            <li className="menu-item"><Link to="/contact">Contact</Link></li>
            {/*<li className="menu-item"><Link to="/werk">Werk</Link></li>*/}
          </ul>
        </div>
      </nav>
    </header>
  );
}

export default Header;
