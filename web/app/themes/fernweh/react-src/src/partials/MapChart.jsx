import React, { memo, useState, useEffect } from 'react';
import {
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography,
} from 'react-simple-maps';
import Axios from 'axios';

const geoUrl = 'https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json';

const MapChart = (props) => {
  let countries = '';
  if (props.countries) {
    countries = props.countries;
  }
  const { setTooltipContent } = props;
  const countryClass = '#D6D6DA';
  const [page, setPage] = useState({});

  // useEffect(() => {
  //     const fetchPage = () => Axios
  //         .get(geoUrl)
  //         .then((res) => {
  //             const page = res.data.objects.ne_110m_admin_0_countries.geometries;
  //
  //             let countryCodeMap = page.find(o => o.properties.ISO_A2 === 'BA').properties.ISO_A2.toLowerCase();
  //
  //             let countryItem = props.countries.find(o => o.country_code === countryCodeMap);
  //
  //             console.log(countryItem);
  //
  //             setPage(page);
  //         })
  //         .catch((err) => console.log(err));
  //     fetchPage();
  // }, [props.countries]);
  //
  // // let countryItem2 = page.find(o => o.properties.ISO_A2 === 'HR');

  //

  const activeCountry = '';

  const results = (
    <>
      <section className="world-map">
        <div className="container">
          <h2 className="block-title align-center">De landen die ik heb bezocht</h2>
          <ComposableMap data-tip="" projectionConfig={{ scale: 500 }}>
            <ZoomableGroup>
              <Geographies geography={geoUrl}>
                {({ geographies }) => geographies.map((geo) => (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    onMouseEnter={() => {
                      const countryCode = geo.properties.ISO_A2.toLowerCase();
                      let countryItem = props.countries.find((o) => o.country_code === countryCode);
                      const { NAME } = geo.properties;
                      setTooltipContent(`${NAME}`);
                    }}
                    center={[-100, -150]}
                    onMouseLeave={() => {
                      setTooltipContent('');
                    }}
                    style={{
                      default: {
                        fill: '#EEE',
                        outline: 'none',
                      },
                      hover: {
                        fill: '#000',
                        outline: 'none',
                      },
                      pressed: {
                        fill: '#E42',
                        outline: 'none',
                      },
                    }}
                  />
                ))}
              </Geographies>
            </ZoomableGroup>
          </ComposableMap>
        </div>
      </section>
    </>
  );

  return results;
};

export default memo(MapChart);
