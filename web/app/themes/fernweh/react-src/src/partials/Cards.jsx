import React, {useState, useEffect} from 'react';
import Axios from 'axios';

import Card from './Card/Card';
import Spinner from './Spinner';

const Cards = (props) => {

    const [items, setitems] = useState({});
    let path = props.route.match.path;
    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/`;

    switch(path) {
        case '/landen':
            url += `countries?per_page=100`;
            break;
        case '/landen/:catid':
            let countryId = props.countryId;
            url += `blog?countries=${
                countryId
            }`;
            break;
        default:
            url += `blog`;
    }

    useEffect(() => {
        Axios
            .get(
                url,
            )
            .then((res) => {
                let items = res.data;
                setitems(items);
            })
            .catch((err) => console.log(err));
    }, []);

    if (
        items === undefined ||
        Object.keys(items).length === 0
    ) {
        return <Spinner/>;
    } else {
        let cards = items.map((item, i) => (
            <Card route={props} item={item} key={i}/>
        ));


        return cards;
    }
};

export default Cards;