import React, {useState, useEffect} from 'react';
import Axios from "axios";

const AboutMe = () => {

    let [items, setItems] = useState({});
    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/theme/fernweh`;

    useEffect(() => {
        Axios
            .get(
                url,
            )
            .then((res) => {
                items = res.data;

                setItems(items);
            })
            .catch((err) => console.log(err));
    }, []);

        const theContent = (<div className="about-me">
            <div
                className="about-me__picture"
                dangerouslySetInnerHTML={{ __html: items.picture }}
            />
            <div className="about-me__content">
                <h2>Mauro Verheem</h2>
                <p>{items.text}</p>
                <a href="/over">Meer over Mauro</a>
            </div>
        </div>);

    return (
        theContent
    );
}

export default AboutMe;