import React from 'react';

function Foot() {
  return (
    <div className="footer">
      <footer>
        <div className="footer">
          <div className="container" />
          <div className="footer__copyright">
            <div className="container">
                            © 2019 - Fernweh is een project van Mauro Verheem
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
export default Foot;
