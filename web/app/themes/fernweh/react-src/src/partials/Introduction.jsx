import React, { useState, useEffect, useContext } from 'react';
import Axios from 'axios';
import Spinner from './Spinner';

const Introduction = (props) => {
  const [page, setPage] = useState({});

  let { slug } = props.props.match.params;
  const { path } = props.props.match;

    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/`;

  switch (path) {
    case '/landen/:catid':
      slug = props.props.match.params.catid;
      url += `countries?slug=${slug}`;
      break;
    case '/blog/:slug':
      slug = props.props.match.params.slug;
        url += `blog?slug=${slug}`;
      break;
    default:
      url +=  `pages?slug=${slug}`;
  }

  useEffect(() => {
    Axios
      .get(url)
      .then((res) => {
        // console.log(res.data[0]);
        const page = res.data[0];

        setPage(page);
      })
      .catch((err) => console.log(err));
  }, []);


  if (
    page === undefined
        || Object.keys(page).length === 0
  ) {
    return <Spinner />;
  }

  return page.introduction;
};

export default Introduction;
