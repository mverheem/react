import React from 'react';

const Hero = (props) => {

      const heroImage = props.image;
      const title = props.title;


    const results = (
        <section className="hero">
            <div className="hero__image">
                <div
                    className="hero__image"
                    dangerouslySetInnerHTML={{ __html: heroImage }}
                />
                <div className="hero-content">
                    {/* <span className="hero__subtitle">{heroSubtitle}</span> */}
                    <h1 className="hero__title">
                        {title}
                    </h1>
                </div>
            </div>
        </section>)

      return results;

};

export default Hero;