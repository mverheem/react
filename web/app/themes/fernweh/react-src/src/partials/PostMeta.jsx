import React from 'react'; 
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

const PostMeta = (props) => {
        let theMeta = (<div className="article-metadata">
        <Moment format="MM/DD/YYYY" className='date'>{props.data.date}</Moment>,
            Leestijd {props.data.date}
        </div>)

    return theMeta;
};

export default PostMeta;