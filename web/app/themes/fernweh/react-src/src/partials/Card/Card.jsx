import React from 'react';
import {Link} from 'react-router-dom';
import Moment from 'react-moment';

const Card = (props) => {
    let route = props.route.route;
    let path = route.match.path;
    const linkPrefix = path === '/blog' ? '/blog/' : path === '/landen' ? '/landen/' : '/blog/';
    let theCard = '';
    let cardImage = props.item.featured_image.blog_fullwidth;
    let title = '';
    let date = props.item.date;
    let slug = props.item.slug;
    let item = props.item;
    let metaData = '';


    switch (path) {
        case '/front':
            metaData = (  <div className="article-metadata">
                <Moment format="MM/DD/YYYY" className="date">{date}</Moment>
            </div>);
            break;
        case '/landen':
        case '/landen/:catid':
            title = item.name;
            break;
        default:
            title = item.title.rendered;
    }

    if (cardImage) {
        theCard = (
            <Link to={linkPrefix + slug} className="grid-item">
                <div
                    className="grid-item__image"
                    dangerouslySetInnerHTML={{__html: cardImage}}
                />
                <div className="grid-item-content">
                    <div
                        className="grid-item__title"
                        dangerouslySetInnerHTML={{__html: title}}
                    />
                    {metaData}
                </div>
            </Link>
        );
    }

    return theCard;
};
export default Card;