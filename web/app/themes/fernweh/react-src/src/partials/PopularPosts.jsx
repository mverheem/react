import React, {useState, useEffect} from 'react';
import Axios from 'axios';
import Card from './Card/Card';
import Spinner from './Spinner';

const PopularPosts = (props) => {

    const [blogs, setBlogs] = useState({});

    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/blog`;

    useEffect(() => {
        Axios
            .get(
                url,
            )
            .then((res) => {
                let blogs = res.data;

                setBlogs(blogs);
            })
            .catch((err) => console.log(err));
    }, []);


    if (
        blogs === undefined ||
        Object.keys(blogs).length === 0
    ) {
        return <Spinner/>;
    } else {
        let cards = blogs.map((item, i) => (

            <Card route={props} item={item} key={i}/>
        ));
        const results = (
            <section className="popular-articles">
                <h2 className="section-title">Populaire artikelen</h2>
                <div className="grid-container grid-container--default">
                    {cards}
                </div>
            </section>)

        return results;
    }
};

export default PopularPosts;