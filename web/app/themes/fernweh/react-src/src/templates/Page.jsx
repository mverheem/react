import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import Axios from 'axios';
import Spinner from '../partials/Spinner';
import Hero from '../partials/Hero';
import TheContent from '../partials/TheContent';
import Breadcrumbs from '../partials/Breadcrumbs';


const Page = (props) => {
  const title = 'Fernweh';
  const [page, setPage] = useState({});

  let { slug } = props.match.params;

  if (slug === '/') {
    slug = 'home';
  }

  useEffect(() => {
    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/pages?slug=${
      slug
    }`;
    const fetchItems = () => Axios
      .get(url)
      .then((res) => {
        const page = res.data[0];
        setPage(page);
      })
      .catch((err) => console.log(err));
    fetchItems();
  }, [slug]);

  if (
    page === undefined
        || Object.keys(page).length === 0
  ) {
    return <Spinner />;
  }
  const results = (
    <>
      <Helmet>
        <title>{ title }</title>
      </Helmet>
      <div className="Page">
        <Hero title={page.title.rendered} image={page.featured_image.hero} props={props} />
        <section className="article-content">
          <div className="container">
            <div className="body-text">
              <Breadcrumbs title={page.title.rendered} />
              <TheContent content={page.introduction} route={props} />
            </div>
          </div>
        </section>
      </div>
    </>
  );
  return results;
};

export default Page;
