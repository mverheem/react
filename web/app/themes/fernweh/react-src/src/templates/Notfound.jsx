import React from 'react';

class NotFound extends React.Component {


  render() {
    return (
      <div className="notfound">
        <div className="content-area">
        <h1>Not Found</h1>
        </div>
      </div>
    )
  }
}
export default NotFound