import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Spinner from '../partials/Spinner';
import Hero from '../partials/Hero';
import TheContent from '../partials/TheContent';
import Breadcrumbs from '../partials/Breadcrumbs';
import Cards from '../partials/Cards';

const CountryPage = (props) => {
  const [page, setPage] = useState({});
  const slug = props.match.params.catid;

  useEffect(() => {
    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/countries?slug=${
        slug
    }`;
    const fetchItems = () => Axios
      .get(url)
      .then((res) => {
        const page = res.data[0];
        setPage(page);
      })
      .catch((err) => console.log(err));
    fetchItems();
  }, []);

  if (
    page === undefined
        || Object.keys(page).length === 0
  ) {
    return <Spinner />;
  }
  const results = (
    <>
      <div className="Page">
        <Hero title={page.name} image={page.featured_image.hero} props={props} />
        <section className="article-content">
          <div className="container">
            <div className="body-text">
              <Breadcrumbs title={page.name} />
              <TheContent content={page.description} route={props} />
              <div className="grid-container grid-container--default">
                <Cards countryId={page.id} route={props} />
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
  return results;
};

export default CountryPage;
