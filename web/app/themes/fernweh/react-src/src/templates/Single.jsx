import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Spinner from '../partials/Spinner';
import AboutMe from '../partials/AboutMe';
import Hero from '../partials/Hero';
import PostMeta from '../partials/PostMeta';
import Breadcrumbs from '../partials/Breadcrumbs';
import FlexibleContent from '../partials/FlexibleContent';


const Single = (props) => {
  const [page, setPage] = useState({});

  const { slug } = props.match.params;
  const base_url = process.env.REACT_APP_BASE_URL;
  let url = `${base_url}/wp-json/wp/v2/blog?slug=${slug}`;

  useEffect(() => {
    Axios
      .get(url)
      .then((res) => {
        const page = res.data[0];
        setPage(page);
      })
      .catch((err) => console.log(err));
  }, []);

  if (
    page === undefined
        || Object.keys(page).length === 0
  ) {
    return <Spinner />;
  }
  const results = (
    <>
      <div className="Page">
        <Hero title={page.title.rendered} image={page.featured_image.hero} props={props} />
        <section className="article-content">
          <div className="container">
            <div className="body-text">
              <Breadcrumbs title={page.title.rendered} />
              <PostMeta data={page} />
              <FlexibleContent flexible_content={page.flexible_content} props={props} />
              <AboutMe />
            </div>
          </div>
        </section>
      </div>
    </>
  );
  return results;
};

export default Single;
