import React, {useState, useEffect} from 'react';
import Axios from 'axios';
import Spinner from '../partials/Spinner';
import Hero from '../partials/Hero';
import TheContent from '../partials/TheContent';
import LatestPosts from "../partials/LatestPosts";
import PopularPosts from '../partials/PopularPosts';

const Front = (props) => {
    const [page, setPage] = useState({});
    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/pages?slug=home`;

    useEffect(() => {
        Axios
            .get(url)
            .then((res) => {

                const page = res.data[0];
                setPage(page);
            })
            .catch((err) => console.log(err));
    }, []);

    if (page === undefined || Object.keys(page).length === 0) {
        return <Spinner/>;
    } else {
        return (
            <>
                <div className="Page">
                    <Hero title='Fernweh' image={page.featured_image.hero} props={props} />
                    <section className="page-content">
                        <div className="container">
                            <TheContent content={page.content.rendered} route={props} />
                            <LatestPosts route={props}></LatestPosts>
                            <PopularPosts route={props}></PopularPosts>
                        </div>
                    </section>
                </div>
            </>)
    }
};

export default Front;