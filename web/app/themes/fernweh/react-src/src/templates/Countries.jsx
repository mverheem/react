import React, { useState, useEffect } from 'react';
import ReactTooltip from "react-tooltip";
import Axios from 'axios';
import Spinner from '../partials/Spinner';
import Hero from '../partials/Hero';
import TheContent from '../partials/TheContent';
import Breadcrumbs from '../partials/Breadcrumbs';
import Cards from '../partials/Cards';
import MapChart from "../partials/MapChart";

const Countries = (props) => {
    const [page, setPage] = useState({});
    const [countries, setCountries] = useState({});
    const [content, setContent] = useState("");
    const base_url = process.env.REACT_APP_BASE_URL;

    useEffect(() => {
        let url = `${base_url}/wp-json/wp/v2/pages?slug=landen`;
        const fetchPage = () => Axios
            .get(url)
            .then((res) => {
                const page = res.data[0];
                setPage(page);
            })
            .catch((err) => console.log(err));
        fetchPage();
    }, []);

    useEffect(() => {
        let urlCountries = `${base_url}/wp-json/wp/v2/countries`;
        const fetchPage = () => Axios
            .get(urlCountries)
            .then((res) => {
                const countries = res.data;
                setCountries(countries);
            })
            .catch((err) => console.log(err));
        fetchPage();
    }, []);

    if (
        page === undefined
        || Object.keys(page).length === 0
    ) {
        return <Spinner />;
    }

    const results = (
            <div className="Page">
                <Hero title={page.title.rendered} image={page.featured_image.hero} props={props} />
                <section className="page-content">
                    <div className="container">
                        <Breadcrumbs title={page.title.rendered} />
                        <TheContent content={page.introduction} route={props} />
                        <MapChart countries={countries} setTooltipContent={setContent} />
                        <ReactTooltip>{content}</ReactTooltip>
                        <div className="grid-container grid-container--default">
                            <Cards countryId={page.id} route={props} />
                        </div>
                    </div>
                </section>
            </div>
    );
    return results;
};

export default Countries;
