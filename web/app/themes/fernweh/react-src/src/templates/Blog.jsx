import React from 'react';
import Cards from '../partials/Cards';

const Blog = (props) => (
    <>
        <div className="Blog">
               <section className="page-content">
                    <div className="grid-container grid-container--full-width">
                        <Cards route={props} />
                    </div>
                </section>
        </div>
    </>
);

export default Blog;