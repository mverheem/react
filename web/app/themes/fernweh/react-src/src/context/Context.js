import React, { useState, useEffect } from "react";
import axios from "axios";

export const Context = React.createContext();

export function Provider({ children }) {
    let intialState = {
        pages: [],
        heading: ""
        // dispatch: action => this.setState(state => reducer(state, action))
    };

    const [state, setState] = useState(intialState);

    const base_url = process.env.REACT_APP_BASE_URL;
    let url = `${base_url}/wp-json/wp/v2/pages`;

    useEffect(() => {
        axios
            .get(
                url
            )
            .then(res => {
                setState({
                    pages: res.data,
                    heading: "Fernweh"
                });
            })
            .catch(err => console.log(err));
    }, []);

    return (
        <Context.Provider value={[state, setState]}>{children}</Context.Provider>
    );
}