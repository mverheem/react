import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Header from "./partials/Header";
import Front from "./templates/Front";
import Blog from "./templates/Blog";
import Single from "./templates/Single";
import CountryPage from "./templates/CountryPage";
import Countries from "./templates/Countries";
import Page from "./templates/Page";
import Footer from "./partials/Footer";
import {Provider} from "./context/Context";

const App = () => {
    return (
        <Provider>
            <Router>
                <Header/>
                <div className="content">
                    <Switch>
                        <Route exact path="/" component={Front}/>
                        <Route path="/blog" component={Blog} exact/>
                        <Route path="/blog/:slug" component={Single} exact/>
                        <Route path="/landen" component={Countries} exact/>
                        <Route path="/:slug" component={Page} exact/>
                        <Route path="/landen/:catid" component={CountryPage} exact/>
                    </Switch>
                </div>
                <Footer/>
            </Router>
        </Provider>
    );
};

export default App;
