(function ($) {

    function getScrollbarWidth(){
        const scrollDiv = document.createElement('div');
        scrollDiv.className = 'scrollbar-measure';
        document.body.appendChild(scrollDiv);
        const scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);
        return scrollbarWidth;
    }
    function setScrollbarWidth(scrollbarWidth){
        const actualPadding = document.body.style.paddingRight
        const calculatedPadding = $(document.body).css('padding-right')
        $(document.body).data('padding-right', actualPadding).css('padding-right', `${parseFloat(calculatedPadding) + scrollbarWidth}px`)
    }
    function resetScrollbarWidth(){
        const padding = $(document.body).data('padding-right')
        if (typeof padding !== 'undefined') {
            $(document.body).css('padding-right', padding).removeData('padding-right');
        }
    }

    function calculateSubmenu() {
        $('.nav_hassubmenu').each(function(){
            var currentMenu = $(this).data('menu');
            var currentOffset = $(this).position().left;
            var totalOffset = currentOffset - 20;

            $('.submenu-collection').find('.submenu[data-menu="' + currentMenu + '"]').css('left', totalOffset);
        });
    }

    calculateSubmenu();

    $(window).on('resize', function() {
        calculateSubmenu();
    });

    var width;
    var leftOffset;

    $('.menu-item a').on('mouseenter', function(){
        $('.submenu').removeClass('active-menu');
        resetScrollbarWidth();
    });

    $('.menu .nav_hassubmenu').on('mouseenter', function () {
        var scrollbar = getScrollbarWidth();
        if ( $('body').css('padding-right') === '0px'){
            setScrollbarWidth(scrollbar);
        }

        var currentMenu = $(this).data('menu');
        $('.submenu').removeClass('active-menu');
        $('.submenu-collection').addClass('submenu-collection--active');
        $('.submenu-collection').find('.submenu[data-menu="'+currentMenu+'"]').addClass('active-menu');
    });

    $('.navbar').on('mouseleave', function(){
        $('.submenu').each(function(){
            $(this).removeClass('active-menu');
        });
    });

    $('.submenu-collection').on('mouseenter', function(){
        var scrollbar = getScrollbarWidth();
        if ( $('body').css('padding-right') === '0px'){
            setScrollbarWidth(scrollbar);
        }

        $(this).addClass('submenu-collection--active');
    });

    $('.submenu-collection').on('mouseleave', function(){
        $('.taalhuis-submenu').each(function(){
            $(this).removeClass('active-menu');
        });
        resetScrollbarWidth();
        resetCourseMenu();
        $(this).removeClass('submenu-collection--active');
    });

    $('.header__wrapper .hamburger').on('click', function(){
        $('body').toggleClass('navbar-active');
        $(this).toggleClass('is-active');
    });

    $('.taalhuis-languages.adults').addClass('active');
    $('.taalhuis-audiences .adults').addClass('active');

    function resetCourseMenu() {
        $('.taalhuis-audiences').find('li').removeClass('active');
        $('.taalhuis-languages').removeClass('active');

        $('.taalhuis-languages').first().addClass('active');
        $('.taalhuis-audiences').find('li').first().addClass('active');
    }

    $('[data-language]').on('mouseenter', function(){
        $('.languagelink').removeClass('active');
        $(this).parent('li').addClass('active');
        $('[data-courselanguage]').removeClass('active');
        var language = $(this).data('language');
        $('[data-courselanguage="'+language+'"]').addClass('active');
    });

})(jQuery);