(function ($) {


    if (window.location.pathname == "/landen/") {

        var countries = Object.keys(countryData);

        $(function () {
            $('.map').load('/app/themes/fernweh/dist/images/map-2.svg');

            $('.map .active').addClass('active');
            $('.map').on('mouseenter', 'path, g', function () {
                if ($(this).attr('class')) {
                    $(this).addClass('hover');
                }
            }).on('click', 'path', function (e) {
                if ($(this).hasClass('active')) {
                    if ($(this).attr('class')) {
                        const countryCode = $(this)[0].id.toUpperCase();

                        var countrySlug = JSON.parse(countryData[countryCode]).slug;


                        window.location.assign('/blog/' + countrySlug);
                    }
                }
            });

            $('.map').append('<div class="tooltip"><img class="tooltip__image" src=""><h2 class="tooltip__title"></h2><p class="tooltip__text"></p></div>');
            var $tooltip = $('.tooltip');
            $('path').hover(function () {
                var t = this.getBoundingClientRect().top,
                    l = this.getBoundingClientRect().left;
                $tooltip.css({"top": t + "px", "left": l + "px"}).show();
            }, function () {
                $tooltip.hide();
            });
            $('path').hover(function () {
                console.log($(this).hasClass('active'));
                if ($(this).hasClass('active')) {
                    let countryCode = $(this)[0].attributes[0].value;
                    let selectedCountry = JSON.parse(countryData[countryCode]);
                    let tooltipTitle = $('.tooltip__title');
                    let tooltipImage = $('.tooltip__image');
                    let tooltipText = $('.tooltip__text');
                    $(tooltipTitle).text(selectedCountry.name);
                    $(tooltipText).text(selectedCountry.text);
                    $(tooltipImage).attr("src", selectedCountry.image[0]);
                } else {
                    $tooltip.hide();
                }
            });
        });

        $(window).load(function () {
            var countryJson = new Array();

            function hasPosts(country) {
                return country.hasposts == true;
            }

            function noPosts(country) {
                return country.hasposts == false;
            }


            $.each(countryData, function (index, countryStat) {
                countryJson.push(JSON.parse(countryStat));
            });

            const countriesWithPosts = countryJson.filter(hasPosts);
            const countriesWithoutPosts = countryJson.filter(noPosts);

            $.each(countriesWithPosts, function (index, country) {
                let countryCode = country.countrycode.toUpperCase();
                $('.map path#' + countryCode).addClass('active');
            });

            $.each(countriesWithoutPosts, function (index, country) {
                let countryCode = country.countrycode.toUpperCase();
                $('.map path#' + countryCode).addClass('visited');
            });
        });
    }
})(jQuery);
