(function ($) {

    $(document).ready(function(){
        $('.hamburger').click(function(){
            $(this).toggleClass('open');
            $('.navigation').toggleClass('open');
            $('.header').toggleClass('menu-open');
        });
    });

})(jQuery);
