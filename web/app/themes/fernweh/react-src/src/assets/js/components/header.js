(function ($) {

    $(document).ready(function(){
        $(".header").hover(
            function () {
                $('.header').addClass('header--hover');
            },
            function () {
                $('.header').removeClass('header--hover');
            }
        );

        $('.search--open').click(function(){
            $('.search').addClass('search--active');
            $('.site-container').addClass('search-open');
        });

        $('.search--close').click(function(){
            $('.search').removeClass('search--active');
            $('.site-container').removeClass('search-open');
        });
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 500) {
            $(".header").addClass("header--fixed");
        } else {
            $(".header").removeClass("header--fixed");
        }
    });

})(jQuery);
