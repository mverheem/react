import './components/hamburger.js';
import './components/header.js';
import './components/map.js';
import './components/navbar.js';

(function ($) {
    $(document).ready(function () {
        $('body').trigger('bless');

        $(window).scroll( function(){

            $('.grid-item').each( function(i){

                var bottom_of_object = $(this).offset().top + $(this).outerHeight() - 300;
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if( bottom_of_window > bottom_of_object ){

                    $(this).animate({'opacity':'1'},500);

                }

            });

        });
    });
})(jQuery);