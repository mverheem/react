<!doctype html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' | ';
        } ?><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|=Merriweather:400,900|Raleway:400,900&display=swap"
          rel="stylesheet">

    <?php
    /*
    // Prefetch voor fonts
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="dns-prefetch" href="//use.typekit.net">
    */ ?>

    <?php wp_head(); ?>
</head>

<body>
<?php include('parts/search.php');

if (is_archive()) {
    if (!($post)) {
        $url = get_home_url();
        wp_safe_redirect($url);
        exit;
    }
}
?>
<div class="site-container">
    <header class="header">
        <a class="header__sitename" href="<?php echo esc_url(home_url('/')); ?>
"><?php bloginfo(); ?></a>
        <div class="header__hamburger">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <?php include('parts/navmenu.php'); ?>
    </header>
    <?php
    if (is_archive()) {
        $curid = get_queried_object()->term_id;
    } else {
        $curid = get_the_ID();
    }
    $isCountryOverview = is_archive('post');
    if ($isCountryOverview) {
        $image_id = carbon_get_term_meta($curid, 'featured_image');
    } else {
        $image_id = carbon_get_post_meta($curid, 'featured_image');
    }

    if (empty($image_id)) {
        $hasHeroImage = ' content--hero';
    }
    ?>
    <div class="content">
