<?php

include( 'lib/carbonfields.php' );
include( 'lib/utils.php' );
include( 'lib/init.php' );
include( 'lib/cleanup.php' );
include( 'lib/branding.php' );
include( 'lib/theme.php' );
include( 'lib/tinymce_buttons.php' );
include( 'lib/custom_taxonomies.php' );
include( 'lib/custom_post_types.php' );
include( 'lib/postviews.php' );
include( 'lib/responsive_images.php' );
include( 'lib/breadcrumbs.php' );
include( 'lib/rest_api.php' );


// change comment form fields order
add_filter( 'comment_form_fields', 'mo_comment_fields_custom_order' );
function mo_comment_fields_custom_order( $fields ) {
    $comment_field = $fields['comment'];
    $author_field = $fields['author'];
    $email_field = $fields['email'];
    unset( $fields['comment'] );
    unset( $fields['author'] );
    unset( $fields['cookies'] );
    unset( $fields['email'] );
    unset( $fields['url'] );
    $fields['comment']  = $comment_field;
    $fields['author'] = $author_field;
    $fields['email'] = $email_field;
    return $fields;
}
