<?php

//======================================================================
// Custom post types
//======================================================================
// Register Custom Post Type


add_action('init', 'cpt_resources');
function cpt_resources() {
    $labels = array(
        'name' => _x('Blogs', 'snt'),
        'singular_name' => _x('Blog', 'snt'),
        'add_new' => _x('Add Item', 'snt'),
        'add_new_item' => __('Add Item'),
        'edit_item' => __('Edit Item'),
        'new_item' => __('New Item'),
        'view_item' => __('View Item'),
        'search_items' => __('Search Item'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'taxonomies' => array('countries', 'category'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail', 'editor' ),
        'show_in_rest'          => true,
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'rest_base'             => 'blog',
    );

    register_post_type( 'blog' , $args );

}

function change_link( $permalink, $post ) {

    if( $post->post_type == 'blog' ) {
        $categories = get_the_terms( $post, 'category' );
        $countries = get_the_terms( $post, 'countries' );
        $term_slug = '';

        if( ! empty( $countries ) ) {
            foreach ( $countries as $term ) {
                // The featured resource will have another category which is the main one
                if( $term->slug == 'featured' ) {
                    continue;
                }
                $term_slug = $term->slug;
                break;
            }
            $permalink = get_home_url() ."/blog/landen/" . $term_slug . '/' . $post->post_name;
        }

        if( ! empty( $categories ) ) {
            foreach ( $categories as $term ) {
                $term_slug = $term->slug;
                break;
            }
            $permalink = get_home_url() ."/blog/" . $term_slug . '/' . $post->post_name;

        }


    }
    return $permalink;
}
add_filter('post_type_link',"change_link",10,2);

?>