<?php

namespace Starter\TinyMCE;

//======================================================================
// TinyMCE Buttons
//======================================================================

/**
 * TinyMCE sensible default buttons
 */
function get_buttons(){
	return array( 'styleselect', 'style_formats', 'bold', 'italic', 'bullist', 'numlist', 'blockquote', 'link', 'unlink', 'fullscreen', 'pastetext', 'removeformat', 'charmap', 'undo', 'redo' );
}

/**
 * ACF TinyMCE Toolbars
 */
add_filter( 'acf/fields/wysiwyg/toolbars', function( $toolbars ) {
	unset( $toolbars['Full' ] );
	unset( $toolbars['Basic' ] );

	$toolbars[ 'Standaard' ]      = array();
	$toolbars[ 'Standaard' ][1]   = get_buttons();

	$toolbars[ 'Custom' ]         = array();
	$toolbars[ 'Custom' ][1]      = array('italic', 'bold', 'underline', 'link', 'unlink', 'undo', 'redo' );

	return $toolbars;
} );

/**
 * TinyMCE Quicktag settings
 */
add_filter('quicktags_settings', function( $qtInit ) {
    $qtInit['buttons'] = 'strong,em,link,block,img,ul,ol,li,more,close,dfw';
    return $qtInit;
} );

/**
 * TinyMCE Button bar 1
 */
add_filter( 'mce_buttons', function( $buttons ) {
	return get_buttons();
});

/**
 * TinyMCE Button bar 2
 */
add_filter( 'mce_buttons_2', function( $buttons ) {
	return array();
});

/**
 * TinyMCE Custom styles dropdown
 */
add_filter( 'tiny_mce_before_init', function( $settings ) {
	$style_formats = array(
		array(
			'title' => 'Alinea',
			'block' => 'p'
		),
		array(
			'title' => 'Intro',
			'block' => 'p',
			'classes' => 'intro'
		),
		array(
			'title' => 'Kopjes',
			'items' => array(
				array(
					'title' => 'Titel 1',
					'block' => 'h2',
					'classes' => 'heading__1'
				),
				array(
					'title' => 'Titel 2',
					'block' => 'h3',
					'classes' => 'heading__2'
				)
			)
		),

	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;
} );

