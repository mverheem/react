<?php

namespace Starter\Utils;

//======================================================================
// Utils
//======================================================================

function excerpt( $limit ) {
	return wp_trim_words( get_the_excerpt(), $limit, '&hellip;' );
}

/**
 * Get Youtube video ID from URL
 *
 * @param string $url
 * @return mixed Youtube video ID or FALSE if not found
 */
function get_yt_id( $url ) {
	$parts = parse_url( $url );
	if( isset( $parts['query'] ) ) {
		parse_str( $parts['query'], $qs );
		if( isset( $qs['v'] ) ) {
			return $qs['v'];
		} else if( $qs['vi'] ) {
			return $qs['vi'];
		}
	}
	if( isset( $parts['path'] ) ) {
		$path = explode( '/', trim( $parts['path'], '/' ) );
		return $path[count( $path ) -1];
	}

	return false;
}