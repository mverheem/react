<?php

namespace Starter\Init;

use Starter\Assets;

//======================================================================
// Initialize
//======================================================================

if ( ! function_exists('__t')) {
    function __t()
    {
        return get_template_directory_uri() . '/';
    }
}

if ( ! function_exists('mix')) {
    function mix( $path )
    {
        $pathWithOutSlash = ltrim($path, '/');
        $pathWithSlash    = '/' . ltrim($path, '/');
        $manifestFile     = get_template_directory() . '/mix-manifest.json';
		// No manifest file was found so return whatever was passed to mix().
        if ( ! $manifestFile) {
            return __t() . $pathWithOutSlash;
        }
        $manifestArray = json_decode(file_get_contents($manifestFile), true);
        if (array_key_exists($pathWithSlash, $manifestArray)) {
            return __t() . ltrim($manifestArray[$pathWithSlash], '/');
        }
		// No file was found in the manifest, return whatever was passed to mix().
        return __t() . $pathWithOutSlash;
    }
}

add_action( 'after_setup_theme', function() {
	// Soil features
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-disable-trackbacks' );
	add_theme_support( 'soil-jquery-cdn' );
	add_theme_support( 'soil-js-to-footer' );
	add_theme_support( 'soil-nav-walker' );
	//add_theme_support('soil-disable-asset-versioning');
	//add_theme_support('soil-google-analytics', 'UA-XXXXX-Y');
	//add_theme_support('soil-nice-search');
	//add_theme_support('soil-relative-urls');

	// Add post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', ['caption', 'gallery', 'search-form'] );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus(array(
		'primary_navigation' => __( 'Hoofdmenu', 'starter' ),
		'footer' => __( 'Footer', 'starter' )
	));
} );

/**
 * Theme assets
 */
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style( 'starter/css', mix('/dist/css/main.css'), false, null );

	wp_enqueue_script( 'starter/js', mix('/dist/js/main.js'), ['jquery'], null, true );

	// Onderstaande twee regels zijn vervanging van bovenstaande regel indien je gebruik gaat maken van de vendor-js (zie webpack.mix.js)
	// wp_enqueue_script( 'vendor', get_template_directory_uri() . '/dist/js/vendor.js', ['jquery'], null, true );
	// wp_enqueue_script( 'starter/js', get_template_directory_uri() . '/dist/js/main.js', ['jquery', 'vendor'], null, true );

	wp_localize_script( 'starter/js', 'WPURLS', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}, 100 );

add_image_size( 'hero', 1680, 500, true );