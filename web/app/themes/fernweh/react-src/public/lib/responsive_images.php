<?php

/*
    wp_get_responsive_image($ID, $SIZES, $IMAGESIZE);
    $ID = The ID of the image you want to show
    $SIZES = The name of the registered sizes array you want to use, for instance '4_column', of 'full'
    $IMAGESIZE = The largest of the registered image sizes you want to use, for instance 'large', or maybe 'medium'

    EXAMPLE:
    <?php echo wp_get_responsive_image($image['ID'], '1_column', 'huge'); ?>

    EXAMPLE 2 (uses all image sizes prefixed with 'site_header' and uses the sizes attr defined as 'site_header' ):
    <?php echo wp_get_responsive_image($post_thumbnail_id, 'site_header'); ?>

*/

// Extra resize to fit images
//add_image_size('event_overview', 391, 260, true);
//
//add_image_size('blog-overview_xs', 110, 97, true);
//add_image_size('blog_overview_md', 275, 200, true);
//add_image_size('blog_overview_xl', 550, 400, true);
//
//add_image_size('blog_single', 594, 449, true);
//add_image_size('course_overview', 36, 36, true);
//add_image_size('material_overview', 173, 234, true);
//add_image_size('teacher_overview', 202, 252, true);
//
//add_image_size('module_teaser', 405, 250, true);
//add_image_size('module_teaser-xs', 478, 264, true);

add_image_size('article_image', 916, 731, true);

add_image_size('blog_halfwidth', 405, 250, true);
add_image_size('blog_fullwidth', 826, 620, true);

// Cropped images
//add_image_size('site_header_xs', 360, 185, true);
//add_image_size('site_header_sm', 360, 185, true);
//add_image_size('site_header_md', 360, 185, true);
add_image_size('hero', 2000, 1200, true);

function wp_get_responsive_image(
    $attachment_id,
    $name = 'default',
    $size = 'original',
    $attr = ''
) {
    if (!$attachment_id) {
        return false;
    }

    $src = wp_get_attachment_image_src($attachment_id, $size);
    if (!$src) {
        return false;
    }

    $default_attr = array(
        'srcset' => apply_filters(
            'responsive_image_srcset',
            false,
            $name,
            $attachment_id
        ),
        'sizes' => apply_filters(
            'responsive_image_sizes',
            false,
            $name,
            $attachment_id
        )
    );

    $attr = wp_parse_args($attr, $default_attr);
    $img = wp_get_attachment_image($attachment_id, $size, false, $attr);

    return $img;
}

// Srcset Attr
function responsive_image_srcset($srcset, $img_name, $attachment_id)
{
    $srcset = '';
    $image_sizes = get_intermediate_image_sizes();
    foreach ($image_sizes as $image_size) {
        if (strpos($image_size, $img_name) === 0) {
            $image_src = wp_get_attachment_image_src(
                $attachment_id,
                $image_size
            );
            $srcset = $srcset . $image_src[0] . ' ' . $image_src[1] . 'w, ';
        }
    }
    if (empty($srcset)) {
        $srcset = wp_get_attachment_image_srcset($attachment_id, 'huge');
    }
    return $srcset;
}
add_filter('responsive_image_srcset', 'responsive_image_srcset', 10, 3);

// Sizes Attr
function responsive_image_sizes($sizes, $img_name, $attachment_id)
{
    $sizes = wp_get_attachment_image_sizes($attachment_id, 'original');
    $meta = wp_get_attachment_metadata($attachment_id);
    $width = $meta['width'];

    if (!$width) {
        return $sizes;
    }

    if ('full' == $img_name) {
        $sizes = '';
        $media_queries = array('100vh');
        $sizes = sprintf('%s', implode(', ', $media_queries));
    }

    if ('teasermodule' == $img_name) {
        $sizes = '';
        $media_queries = array(
            '(max-width: 576px) 510px',
            '(max-width: 576px) and (min-resolution: 161dpi) 1120px',

            '(max-width: 768px) 405px',
            '(max-width: 768px) and (min-resolution: 161dpi) 810px',

            '(max-width: 992px) 405px',
            '(max-width: 992px) and (min-resolution: 161dpi) 810px',

            '(max-width: 1200px) 405px',
            '(max-width: 1200px) and (min-resolution: 161dpi) 810px',

            '1400px',
            '(min-resolution: 161dpi) 1200px'
        );
        $sizes = sprintf('%s', implode(', ', $media_queries));
    }

    if ('blog_overview' == $img_name) {
        $sizes = '';
        $media_queries = array(
            '(max-width: 576px) 110px',
            '(max-width: 576px) and (min-resolution: 161dpi) 220px',

            '(max-width: 768px) 310px',
            '(max-width: 768px) and (min-resolution: 161dpi) 620px',

            '(max-width: 992px) 270px',
            '(max-width: 992px) and (min-resolution: 161dpi) 552px',

            '(max-width: 1200px) 276px',
            '(max-width: 1200px) and (min-resolution: 161dpi) 552px',

            '276px',
            '(min-resolution: 161dpi) 552px'
        );
        $sizes = sprintf('%s', implode(', ', $media_queries));
    }

    if ('site_header' == $img_name) {
        $sizes = '';
        $media_queries = array(
            '(max-width: 576px) 576px',
            '(max-width: 576px) and (min-resolution: 161dpi) 952px',

            '(max-width: 768px) 768px',
            '(max-width: 768px) and (min-resolution: 161dpi) 1536px',

            '(max-width: 992px) 992px',
            '(max-width: 992px) and (min-resolution: 161dpi) 1984px',

            '(max-width: 1200px) 1200px',
            '(max-width: 1200px) and (min-resolution: 161dpi) 2400px',

            '1200px',
            '(min-resolution: 161dpi) 2400px'
        );
        $sizes = sprintf('%s', implode(', ', $media_queries));
    }

    return $sizes;
}
add_filter('responsive_image_sizes', 'responsive_image_sizes', 10, 3);


// Add the default WP image styles in media library
function my_custom_size($sizes)
{
    return array_merge($sizes, array(
        'blog_single' => __('Berichtafbeelding'),
    ));
}

add_filter('image_size_names_choose', 'my_custom_size');


// Remove the default WP image styles in media library
function wp_70048_remove_image_sizes($sizes)
{
    unset($sizes['thumbnail']);
    unset($sizes['medium']);
    unset($sizes['large']);
    unset($sizes['full']);
    return $sizes;
}

add_filter('image_size_names_choose', 'wp_70048_remove_image_sizes');

function custom_image_size() {
    // Set default values for the upload media box
    update_option('image_default_align', 'center' );
    update_option('image_default_size', 'large' );

}
add_action('after_setup_theme', 'custom_image_size');
