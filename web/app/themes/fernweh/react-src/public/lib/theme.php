<?php

namespace Starter\Theme;

//======================================================================
// WordPress
//======================================================================

/**
* Get rid of current_page_parent class on blog page menu item on other post type page
*
* @link (https://wordpress.org/support/topic/why-does-blog-become-current_page_parent-with-custom-post-type)
*
*/
add_filter( 'nav_menu_css_class', function( $classes, $item, $args ) {
	if( !is_category() && !is_tag() ) {
		$blog_page_id = intval( get_option( 'page_for_posts' ) );
		if( $blog_page_id != 0 ) {
			if( $item->object_id == $blog_page_id ) {
				unset( $classes[array_search( 'current_page_parent', $classes )] );
			}
		}
	}
	return $classes;
}, 10, 3 );

/**
 * Clean up the_excerpt()
 */
add_filter( 'excerpt_more', function( $more ) {
	return '&hellip;';
} );

add_filter( 'excerpt_length', function( $length ) {
	return 20;
}, 999 );

/**
 * Register sidebars
 */
add_action( 'widgets_init', function() {
	register_sidebar([
		'name'          => __('Sidebar', 'vvt'),
		'id'            => 'sidebar-sidebar',
		'before_widget' => '<div class="sidebar-widget %1$s %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="sidebar-widget__title">',
		'after_title'   => '</h3>'
	]);

	unregister_widget( 'WP_Widget_Pages' );
	unregister_widget( 'WP_Widget_Calendar' );
	unregister_widget( 'WP_Widget_Archives' );
	unregister_widget( 'WP_Widget_Links' );
	unregister_widget( 'WP_Widget_Meta' );
	unregister_widget( 'WP_Widget_Search' );
	unregister_widget( 'WP_Widget_Categories' );
	unregister_widget( 'WP_Widget_Recent_Posts' );
	unregister_widget( 'WP_Widget_Recent_Comments' );
	unregister_widget( 'WP_Widget_RSS' );
	unregister_widget( 'WP_Widget_Tag_Cloud' );
	unregister_widget( 'WP_Widget_Polls' );
} );

/**
* Wrap embed html with bootstrap responsive embed div
*/
add_filter( 'embed_oembed_html', function( $html, $url, $attr ) {
	if ( ! is_admin() ) {
		return "<div class=\"embed-responsive embed-responsive-16by9\">" . $html . "</div>";
	} else {
		return $html;
	}
}, 10, 3 );

/**
* Allow svg's
*/
add_filter( 'upload_mimes', function( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
} );

/**
* Custom Editor Styles
*/
add_action( 'init', function() {
	add_editor_style( 'dist/css/editor.css' );
} );