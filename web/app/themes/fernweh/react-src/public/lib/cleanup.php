<?php

namespace Starter\Cleanup;

//======================================================================
// Cleanup
//======================================================================

/**
 * Don't show admin bar
 */
add_filter( 'show_admin_bar', '__return_false' );

/**
 * Default link type none
 */
update_option( 'image_default_link_type', 'none' );

/**
 * Remove emoji's
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Remove comments menu item
 */
add_action( 'admin_menu', function() {
	remove_menu_page( 'edit-comments.php' );
} );

/**
 * Low priority WP SEO metabox
 */
add_filter( 'wpseo_metabox_prio', function() {
	return 'low';
} );

/**
 * Remove clutter from main dashboard page.
 *
 * @link http://codex.wordpress.org/Dashboard_Widgets_API#Advanced:_Removing_Dashboard_Widgets
 * @link https://digwp.com/2014/02/disable-default-dashboard-widgets/
 */
add_action( 'wp_dashboard_setup', function() {
	global $wp_meta_boxes;

	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
} );

/**
 * Remove user roles and add opdrachtgever
 */
add_action('admin_menu', function() {
	global $wp_roles;

	$roles_to_remove = array( 'subscriber', 'contributor', 'author', 'editor' );

	foreach ( $roles_to_remove as $role ) {
	    if ( isset( $wp_roles->roles[$role] ) ) {
	        $wp_roles->remove_role( $role );
	    }
	}

	$result = add_role(
		'opdrachtgever',
		__( 'Opdrachtgever' ),
		array(
			'edit_dashboard' => true,
			'export'         => false,
			'import'         => false,
			'manage_links'         => false,
			'manage_options'         => false,
			'moderate_comments'         => false,
			'unfiltered_html'         => false,
			'update_core'  => false,
			'read'         => true,
			'edit_posts'   => true,
			'delete_posts' => true,
			'delete_others_posts' => true,
			'delete_private_posts' => true,
			'delete_published_posts' => true,
			'edit_others_posts' => true,
			'edit_private_posts' => true,
			'edit_published_posts' => true,
			'publish_posts' => true,
			'read_private_posts' => true,
			'delete_others_pages' => true,
			'delete_pages' => true,
			'delete_private_pages' => true,
			'delete_published_pages' => true,
			'delete_others_pages' => true,
			'edit_pages' => true,
			'edit_private_pages' => true,
			'edit_published_pages' => true,
			'publish_pages' => true,
			'read_private_pages' => true,
			'upload_files' => true,
			'manage_categories' => true,
			'delete_themes' => false,
			'edit_theme_options' => false,
			'edit_themes' => false,
			'install_themes' => false,
			'switch_themes' => false,
			'update_themes' => false,
			'delete_themes' => false,
			'edit_theme_options' => false,
			'edit_themes' => false,
			'install_themes' => false,
			'switch_themes' => false,
			'update_themes' => false,
			'create_roles' => false,
			'create_users' => true,
			'delete_roles' => false,
			'delete_users' => true,
			'edit_roles' => false,
			'edit_users' => true,
			'list_roles' => false,
			'list_users' => true,
			'promote_users' => false,
			'remove_users' => true,
			'restrict_content' => false,
			'activate_plugins' => false,
			'delete_plugins' => false,
			'edit_plugins' => false,
			'install_plugins' => false,
			'update_plugins' => false,
		)
	);
} );

add_action( 'admin_menu', function() {
//	remove_menu_page( 'edit-comments.php' );
//	remove_menu_page( 'edit.php' );
	remove_submenu_page( 'themes.php', 'widgets.php' );
//	remove_submenu_page( 'options-general.php','options-discussion.php' );
});