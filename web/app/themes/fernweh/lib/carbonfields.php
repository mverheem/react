<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'crb_attach_theme_options');
function crb_attach_theme_options()
{
    Container::make('term_meta', 'Category Properties')
        ->where('term_taxonomy', '=', 'countries')
        ->add_fields(array(
            Field::make('text', 'country_code'),
            Field::make('association', 'country_continent')
                ->set_types(array(
                    array(
                        'type' => 'term',
                        'taxonomy' => 'continents',
                    ),
                ))->set_max(1),
            Field::make('color', 'crb_title_color'),
            Field::make('image', 'featured_image', 'Afbeelding'),
            Field::make('text', 'featured_subtitle', 'Subtitel'),
            Field::make('complex', 'country_facts', 'Facts')
//                ->set_layout( 'tabbed-horizontal' )
                ->add_fields(array(
                    Field::make('text', 'title', 'Title'),
                    Field::make('text', 'title_color', 'Title Color'),
                )),
        ));

    Container::make('theme_options', __('Theme Options', 'crb'))
        ->add_tab(__('About'), array(
            Field::make('image', 'about-picture', __('Profile picture')),
            Field::make('textarea', 'about-text', __('About')),
        ))
        ->add_tab(__('Socials'), array(
            Field::make('text', 'linkedin_url', __('Linkedin URL')),
            Field::make('text', 'instagram_url', __('Instagram URL')),
        ));


    Container::make('post_meta', 'Content')
        ->add_tab(__('Hero'), array(
            Field::make('image', 'featured_image', __('Afbeelding')),
            Field::make('text', 'hero_subtitle', __('Subtitel')),
        ))
        ->add_tab(__('Introductie'), array(
            Field::make('textarea', 'introduction', __('Introductie'))
        ))
        ->add_tab(__('Flexibele content'), array(
            Field::make('complex', 'flexible_content', 'Sections')
                ->add_fields('text', 'Text', array(
                    Field::make('rich_text', 'text', 'Text'),
                ))
                ->add_fields('image', 'Afbeelding', array(
                    Field::make( 'radio', 'size', 'Grootte' )
                        ->add_options( array(
                            'normal' => 'Normaal',
                            'fullwidth' => 'Volledige breedte'
                        ) ),
                    Field::make('image', 'image', 'Afbeelding'),
                    Field::make('text', 'caption', 'Beschrijving'),
                ))
                ->add_fields('file_list', 'File List', array(
                    Field::make('complex', 'files', 'Files')
                        ->add_fields(array(
                            Field::make('file', 'file', 'File'),
                        )),
                ))
        ));

}

add_action('after_setup_theme', 'crb_load');
function crb_load()
{
    require_once( dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/vendor/autoload.php'  );
    \Carbon_Fields\Carbon_Fields::boot();
}