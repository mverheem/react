<?php

namespace Starter\Branding;

//======================================================================
// Branding
//======================================================================

/**
 * Custom logo
 */
add_action( 'wp_before_admin_bar_render', function() {
	echo '
	<style type="text/css">
	#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
	background-image: url(' . get_template_directory_uri() . '/dist/images/) !important;
	background-position: center center;
	background-size: cover;
	color:rgba(0, 0, 0, 0);
	}
	#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
	background-position: 0 0;
	}
	</style>
	';
});

/**
 * Custom footer text
 */
add_filter( 'admin_footer_text', function( $text ) {
	$text = '';
	return $text;
});

/**
 * Custom login image
 */
add_action( 'login_enqueue_scripts', function() {
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/dist/images/theme-login_logo.svg);
			padding-bottom: 30px;
			background-size: contain;
		}
	</style>
	<?php
});

