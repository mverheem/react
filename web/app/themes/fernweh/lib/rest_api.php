<?php

add_action('rest_api_init', 'register_fields');
function register_fields()
{
    $objects = array('page', 'blog', 'countries');
    $fields = array('featured_subtitle', 'featured_image', 'flexible_content', 'country_code', 'introduction');
    foreach ($objects as $object) {
        foreach ($fields as $field) {

            register_rest_field(
                $object,
                $field,
                array(
                    'get_callback' => 'get_field',
                    'update_callback' => null,
                    'schema' => null,
                )
            );
        }
    }
}

add_action( 'rest_api_init', 'define_endpoint');
function define_endpoint(){
    register_rest_route( 'theme', '/fernweh', array(
        'methods' => 'GET',
        'callback'        => 'api_method'
    ) );
    register_rest_route( 'aalaap/v1', '/countview/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'aalaap_count_view',
    ) );

}

//function aalaap_count_view( $data ) {
//    $post = get_post( $data['id'] );
//
//    if ( empty( $post ) ) {
//        return new WP_Error( 'aalaap_post_not_found', 'Invalid post', array( 'status' => 404 ) );
//    }
//
//    // Now update the ACF field (or whatever you wish)
//    $count = (int) get_field('views', $post->ID);
//    $count++;
//    update_field('views', $count, $post->ID);
//
//    return new WP_REST_Response($count);
//}

function api_method($data) {
    $picture =  wp_get_attachment_image(carbon_get_theme_option( 'about-picture' ), 'thumbnail');
    $text = carbon_get_theme_option( 'about-text' );

    $data = array (
        'picture' => $picture,
        'text' => $text
    );

    return  rest_ensure_response($data);
}


function getImageSizes()
{
    global $_wp_additional_image_sizes;
    foreach ($_wp_additional_image_sizes as $key => $size) {
        $sizes[] = $key;
    }
    return $sizes;
}

function get_field($object, $field_name, $request)
{

    $objectId = $object['id'];
    $sizes = getImageSizes();
    $term = '';
    $carbonField = '';
    if (array_key_exists('taxonomy', $object)) {
        $term = $object['taxonomy'];
    }

    if ($term) {
        $carbonField = carbon_get_term_meta($objectId, $field_name);

    } else {
        $carbonField = carbon_get_post_meta($objectId, $field_name);
    }


    foreach ($sizes as $size) {
        $featuredImages[$size] = wp_get_responsive_image($carbonField, $size, "", array("class" => "grid-item__image"));
    }

    if ($carbonField && $field_name == 'featured_image') {
        $fieldValue = $featuredImages;
    } else if ($carbonField && $field_name == 'flexible_content') {
        foreach ($carbonField as $key => $value) {
            if (array_key_exists('image', $carbonField[$key])) {
                $articleImage = wp_get_responsive_image($value['image'], 'article_image', "", array("class" => "grid-item__image"));
                $carbonField[$key]['image'] = $articleImage;

            }
        }
        $fieldValue = $carbonField;
    } else {
        $fieldValue = $carbonField;
    }

    return $fieldValue;
}