<?php
//
////======================================================================
//// Custom taxonomies
////======================================================================
//
//// Register Custom Taxonomy
//function custom_taxonomy() {
//
//	$labels_countries = array(
//		'name'                       => _x( 'Landen', 'Taxonomy General Name', 'text_domain' ),
//		'singular_name'              => _x( 'Land', 'Taxonomy Singular Name', 'text_domain' ),
//		'menu_name'                  => __( 'Land', 'text_domain' ),
//		'all_items'                  => __( 'All Items', 'text_domain' ),
//		'parent_item'                => __( 'Parent Item', 'text_domain' ),
//		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
//		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
//		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
//		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
//		'update_item'                => __( 'Update Item', 'text_domain' ),
//		'view_item'                  => __( 'View Item', 'text_domain' ),
//		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
//		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
//		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
//		'popular_items'              => __( 'Popular Items', 'text_domain' ),
//		'search_items'               => __( 'Search Items', 'text_domain' ),
//		'not_found'                  => __( 'Not Found', 'text_domain' ),
//		'no_terms'                   => __( 'No items', 'text_domain' ),
//		'items_list'                 => __( 'Items list', 'text_domain' ),
//		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
//	);
//	$args_countries = array(
//		'labels'                     => $labels_countries,
//		'hierarchical'               => true,
//		'public'                     => true,
//		'show_ui'                    => true,
//		'show_admin_column'          => true,
//        'show_in_nav_menus'          => true,
//        'show_tagcloud'              => true,
//        'rewrite' => array( 'slug' => 'blog',
//        'with_front' => false ),
//    );
//
//	$labels_continents = array(
//		'name'                       => _x( 'Werelddelen', 'Taxonomy General Name', 'text_domain' ),
//		'singular_name'              => _x( 'Werelddeel', 'Taxonomy Singular Name', 'text_domain' ),
//		'menu_name'                  => __( 'Werelddeel', 'text_domain' ),
//		'all_items'                  => __( 'All Items', 'text_domain' ),
//		'parent_item'                => __( 'Parent Item', 'text_domain' ),
//		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
//		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
//		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
//		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
//		'update_item'                => __( 'Update Item', 'text_domain' ),
//		'view_item'                  => __( 'View Item', 'text_domain' ),
//		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
//		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
//		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
//		'popular_items'              => __( 'Popular Items', 'text_domain' ),
//		'search_items'               => __( 'Search Items', 'text_domain' ),
//		'not_found'                  => __( 'Not Found', 'text_domain' ),
//		'no_terms'                   => __( 'No items', 'text_domain' ),
//		'items_list'                 => __( 'Items list', 'text_domain' ),
//		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
//	);
//	$args_continents = array(
//		'labels'                     => $labels_continents,
//		'hierarchical'               => false,
//		'public'                     => true,
//		'show_ui'                    => true,
//		'show_admin_column'          => true,
//		'show_in_nav_menus'          => true,
//		'show_tagcloud'              => true,
//	);
//    register_taxonomy( 'countries', array( 'blog' ), $args_countries     );
//    register_taxonomy( 'continents', array( 'blog' ), $args_continents );
//
//}
//add_action( 'init', 'custom_taxonomy', 0 );


function countries()
{
    $labels = array(
        'name' => _x('Landen', 'Taxonomy General Name', 'snt'),
        'singular_name' => _x('Land', 'Taxonomy Singular Name', 'snt'),
        'menu_name' => __('Landen', 'snt'),
        'all_items' => __('All Items', 'snt'),
        'parent_item' => __('Parent Item', 'snt'),
        'parent_item_colon' => __('Parent Item:', 'snt'),
        'new_item_name' => __('New Item Name', 'snt'),
        'add_new_item' => __('Add New Item', 'snt'),
        'edit_item' => __('Edit Item', 'snt'),
        'update_item' => __('Update Item', 'snt'),
        'view_item' => __('View Item', 'snt'),
        'separate_items_with_commas' => __('Separate items with commas', 'snt'),
        'add_or_remove_items' => __('Add or remove items', 'snt'),
        'choose_from_most_used' => __('Choose from the most used', 'snt'),
        'popular_items' => __('Popular Items', 'snt'),
        'search_items' => __('Search Items', 'snt'),
        'not_found' => __('Not Found', 'snt'),
        'no_terms' => __('No items', 'snt'),
        'items_list' => __('Items list', 'snt'),
        'items_list_navigation' => __('Items list navigation', 'snt'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => array('slug' => 'blog/landen'),
        'show_in_rest' => true,
        'query_var' => false
    );
    register_taxonomy('countries', array('blog'), $args);
}

add_action('init', 'countries', 0);
?>